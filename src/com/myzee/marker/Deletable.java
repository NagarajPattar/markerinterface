package com.myzee.marker;

public interface Deletable {
	// this is the interface to indicate the entity can be deleted from database or not
}
