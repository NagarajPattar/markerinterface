package com.myzee.marker;

public class CustomMarkerInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EmployeeDAO edao = new EmployeeDAO();
		Employee emp = new Employee("abc", 123);
		
		int isDeletable = edao.delete(emp);
		
		if(isDeletable == 1) 
			System.out.println("employee deleted from database");
		else 
			throw new RuntimeException("employee not deletable from database, should implement com.myzee.marker.Deletable interface");
	}

}
